from mesa import Agent, Model
from mesa.time import RandomActivation
import math
import networkx as nx
from mesa.datacollection import DataCollector
from mesa.space import MultiGrid
import random
import numpy as np

import logging
_log = logging.getLogger(__name__)


list_of_destinations = [(1, "hospital", (80,70)), (2, "mall", (4,5)), (3, "office", (50,50))]

class BCNAgent(Agent):

  def __init__(self, unique_id, model, destination, initial_pos):
    super(BCNAgent, self).__init__(unique_id, model)
    self.destination = destination
    self.time = 0
    self.pollution = random.randint(1,5)
    self.path = [initial_pos]
    self.done = 0
    self.total_pollution = 0

  def calculatePath(self, destination):
    possible_pos = self.model.grid.get_neighborhood(self.pos, moore = False, include_center = False, radius = 1)
    distance_to_dest = 9999999
    new_pos = self.pos
    for dest_a in list_of_destinations:
      for dest_b in possible_pos:
        if dest_a[2] == dest_b and self.destination != dest_a[2]:
          possible_pos.pop(possible_pos.index(dest_b))

    for pos in possible_pos:
      if abs(self.destination[0] - pos[0]) + abs(self.destination[1] - pos[1]) < distance_to_dest:
        new_pos = pos
        distance_to_dest = abs(self.destination[0] - pos[0]) + abs(self.destination[1] - pos[1])
    return new_pos

  def step(self):
    if self.pos != self.destination:
      new_position = self.calculatePath(self.destination)
      self.model.grid.move_agent(self, new_position)
      self.path.append(new_position)
      self.time += 1
      self.total_pollution += self.pollution
    elif self.done == 0:
      f = open("output.txt", "a")
      f.write("agent " + str(self.unique_id) + " path is " + str(self.path) + "time " + str(self.time) + " pollution " + str(self.total_pollution))
      f.write("\n") 
      self.done = 1
      f.close()

class BCNModel(Model):

  def __init__(self, N, width = 10, height = 10):
    _log.info("Initalizing model")
    self.num_agents = N
    self.grid = MultiGrid(width, height, False)
    self.schedule = RandomActivation(self)
    f = open("output.txt", "w")
    f.close()
    
    _log.info("Initalizing agents")
    for i in range(self.num_agents):
      choose_destination = random.randrange(1, len(list_of_destinations) + 1)
      for dest in list_of_destinations:
        if dest[0] == choose_destination:
          destination = dest[2]

      x = random.randrange(self.grid.width)
      y = random.randrange(self.grid.height)
      initial_pos = (x, y)
      a = BCNAgent(i, self, destination, initial_pos)
      self.schedule.add(a)
      self.grid.place_agent(a, (x,y))

  def step(self):
    _log.debug("Stepping model...")
    self.schedule.step()
