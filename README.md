# README #

### What is this repository for? ###

* Code for the Social simulation projects for Planetary wellbeing
* Kanban board available on Trello: https://trello.com/b/eqy3sbXd/

### How do I get set up? ###

* Download and run miniconda installer:

`$ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`

`$ ./Miniconda3-latest-Linux-x86_64.sh`

* Create the conda environment and install all required packages:

`$ ./createEnv.sh`

* Run model

`$ python main.py`

* How to run tests

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

Please configure your editor to insert spaces instead of tabs


