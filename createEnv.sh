#!/bin/bash

# Init functions
CONDA_BASE=$(conda info --base)
source $CONDA_BASE/etc/profile.d/conda.sh

# Remove existing environment
conda deactivate
conda remove --name bwf --all

# Create from scratch
conda create -n bwf python=3.7 -y
conda install -n bwf matplotlib ipython numpy scipy pandas cython -y

conda activate bwf
pip install mesa coloredlogs
